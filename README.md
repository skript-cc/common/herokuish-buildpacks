# Using buildpacks with Herokuish

The aim of this repository is to investigate the use of Heroku buildpacks as 
production ready docker containers (for php). An important focus lies on using
the same images for local development.

## Building images with buildpack

```sh
# Build new image
bin/build.sh php-buildpack:latest $(pwd)

# Start the web process with the new image
bin/start.sh
```

Using docker-compose

```sh
# Build the image (optional)
docker-compose build --pull

# Run the image
docker-compose 
```

## What's happening here? 

When building a new image this folder gets mounted in a build container. The source
is copied to /app (inside the container) and the buildpack does it things there.
It installs php, configures a server (apache or nginx) and performs a container
install. At the end the build script turns the build container into an image.

The second command runs the image in a new container starting a web process by
defining `herokuish procfile start web` as its entrypoint.

## Can I do local development in this container?

For local development we would normally mount the /app directory to a host
directory. The difficulty here is that the buildpack adds some essential stuff
to the /app directory. When we mount a local directory this stuff (that lives
inside the container) gets overwritten.

It turns out that this can be bypassed by mounting the host inside a subdirectory
of the app container.

Another issue is that our file permissions get screwed up after starting a web
container. This can also be bypassed by adding a user while starting the
container (see bin/add-hoster-user.sh and bin/start.sh).

## Some thoughts for deployment

Slugs are a way to export the application after it is build. With the heroku 
stuff stripped from it, it might even be useful to deploy to other hosting
environments.

```sh
docker run --rm -ti php-buildpack bash

# Inside container that runs a custom image
herokuish slug generate && herokuish slug export > slug.tar.gz
```

https://devcenter.heroku.com/articles/slug-compiler