#!/bin/bash

NAME="$1"
SOURCE="$2"

docker pull gliderlabs/herokuish

# Build docker container from source dir

ID=$(docker run -d -v $SOURCE:/tmp/app gliderlabs/herokuish /build)

# Attach to build container to display log

docker attach $ID

if (($? != 0)); then
	exit 1
fi

# Wait for container to finish

test $(docker wait $ID) -eq 0

# Commit changes to new tag

docker commit $ID $NAME > /dev/null

# Remove build container

docker rm $ID