#!/bin/bash

ENV_FILE=.env.${ENV_NAME:-prod}

set +a
source ${ENV_FILE}
set -a

echo "Using $ENV_FILE"

vendor/bin/heroku-php-apache2 ${DOCROOT:-public/}