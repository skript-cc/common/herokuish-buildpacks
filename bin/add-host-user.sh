#!/bin/bash

if [ -n "$USER" ] ; then
    
    if [ -n "$USER_DIR" ] ; then
        USER_UID=$(stat -c "%u" ${USER_DIR})
    fi
    
    USER_UID=${USER_UID:-1000}
    echo "User $USER ($USER_UID)"
    
    adduser \
        --no-create-home \
        --disabled-password \
        --gecos "" \
        --quiet \
        --uid ${USER_UID:-1000} \
        $USER
fi