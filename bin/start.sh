#!/bin/bash

MNT_DIR=/app/dev

# This is starting a development web server
docker run \
    --rm \
    -ti \
    -e USER=web \
    -e USER_DIR=${MNT_DIR} \
    -e PORT=5000 \
    -p 8080:5000 \
    -e DOCROOT=dev/public \
    -e ENV_NAME=dev \
    -v $(pwd):${MNT_DIR} \
    php-buildpack \
    bash -c "${MNT_DIR}/bin/add-host-user.sh && herokuish procfile start web"