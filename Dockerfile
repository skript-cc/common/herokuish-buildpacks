ARG HEROKUISH_VERSION=latest

# BUILD STAGE

FROM gliderlabs/herokuish:${HEROKUISH_VERSION} AS builder
ARG CODE_SRC=.
ARG CODE_DEST=/tmp/app
COPY ${CODE_SRC} ${CODE_DEST}
ENV USER=herokuishuser
RUN /build

# FINAL BUILD

FROM gliderlabs/herokuish:${HEROKUISH_VERSION}
COPY --chown=herokuishuser:herokuishuser --from=builder /app /app
ENV PORT=5000
ENV USER=herokuishuser
EXPOSE 5000
CMD /start web